import React, { useEffect } from 'react'
import {padTime} from '../utility'
import { useSelector,useDispatch } from 'react-redux';
import {setMinutes,setSeconds,setSecondsToZero,stop, setMillisecond, setMillisecondToZero} from './../actions/stopWatchActions'
import { useState } from 'react';
function Timer() {
  
  let {minutes}=useSelector(state=>state)
  let {seconds}=useSelector(state=>state)
  let {milliseconds}=useSelector(state=>state)
  const isRunning=useSelector(state=>state.isRunning)
  const dispatch=useDispatch()

  useEffect(()=>{
    let timerId;
    if(isRunning===true){
      if(minutes===59 && seconds===59){
        dispatch(stop())
      }
      timerId=setInterval(()=>{
        if(milliseconds+1 >= 99){
          dispatch(setSeconds())
          dispatch(setMillisecondToZero())
        }else{
          dispatch(setMillisecond())
        }

        if(seconds+1===60){
          dispatch(setMinutes())
          dispatch(setSecondsToZero())
        }
        
      },10)
      return ()=>window.clearInterval(timerId)
    }

  },[isRunning,minutes,seconds,milliseconds,dispatch])

  return (
    <div className="timer">
        <span>{padTime(minutes)}</span>
        <span>:</span>
        <span>{padTime(seconds)}</span>
        <span>:</span>
        <span>{padTime(milliseconds)}</span>
    </div>
  )
}

export default Timer