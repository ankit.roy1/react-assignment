import React from 'react'

function Button(props) {
  return (
    <button className={props.theme} onClick={props.handleSubmit}>{props.children}</button>
  )
}

Button.defaultProps = {
    theme: "buttons"
}

export default Button