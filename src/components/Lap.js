import { useSelector } from 'react-redux';
import {padTime} from "./../utility"
function Lap(){
    const {lap}=useSelector(state=>state)
    return(
        <div>
            {lap.length > 0 && <h1>Recorded Laps...</h1>}
            { lap.map((curr,index)=>{
                return (
                    <div key={index} className='lap-item'>
                        <h3>{`#${index+1} - ${padTime(curr.minutes)}:${padTime(curr.seconds)}:${padTime(curr.milliseconds)}`}</h3>        
                    </div>
                    )
                })
            }
        </div>
    )
}

export default Lap;
