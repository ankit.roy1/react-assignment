import React from 'react'
import Button from './Button'
import {
  start,
  stop, 
  lap,
  setMillisecondToZero,
  setMinutesToZero,
  setSecondsToZero,
  resetLaps
} from "./../actions/stopWatchActions"
import {useDispatch, useSelector} from "react-redux"
function ButtonGroup() {
  const dispatch=useDispatch()
  const running=useSelector(state=>state.isRunning)
  let {minutes,seconds,milliseconds} =useSelector(state=>state)

  const handleStart=()=>{
    dispatch(start())
  }
  
  const handleStop=()=>{
    dispatch(stop())
  }

  const handleReset=()=>{
    if(!running){
      dispatch(setMillisecondToZero())
      dispatch(setSecondsToZero())
      dispatch(setMinutesToZero())
      dispatch(resetLaps())
    }
  }

  const handleLap=()=>{
    dispatch(lap({minutes,seconds,milliseconds}))
  }
  
  return (
    <div className="buttons">
        {!running && <Button handleSubmit={handleStart}>Start</Button>}
        { running && <Button handleSubmit={handleStop}>Stop</Button>}
        <Button handleSubmit={handleLap}>Lap</Button>
        <Button handleSubmit={handleReset}>Reset</Button>
    </div>
  )
}

export default ButtonGroup