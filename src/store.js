import {createStore} from "redux"
import {stopWatchReducer} from "./reducer/stopWatchReducer"

const store=createStore(stopWatchReducer)

store.subscribe(()=>{
    console.log(store.getState())
})
export {store}