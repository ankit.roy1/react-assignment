const initialState={
    minutes:0,
    seconds:0,
    milliseconds:0,
    isRunning:false,
    lap:[]
}

function stopWatchReducer(state=initialState,action){
    switch(action.type){
        case 'START':
            return{
                ...state,
                isRunning:true
            }

        case 'STOP':
            return{
                ...state,
                isRunning:false
            }
        case 'LAP':
            return{
                ...state,
                lap:[...state.lap,action.payload]
            }

        case 'RESET_LAPS':
            return{
                ...state,
                lap:[]
            }
        case 'SET_MINUTES':
            return{
                ...state,
                minutes:state.minutes + 1 
            }
        case 'SET_MINUTES_TO_ZERO':
            return{
                ...state,
                minutes:0 
            }
        case 'SET_SECONDS':
            return{
                ...state,
                seconds:state.seconds + 1
            }
        case 'SET_SECONDS_TO_ZERO':
            return{
                ...state,
                seconds:0
            }
        case 'SET_MILLISECOND':
            return{
                ...state,
                milliseconds:state.milliseconds + 1 
            }
        case 'SET_MILLISECOND_TO_ZERO':
            return{
                ...state,
                milliseconds:0
            }
        default:
            return state
    }
}

export {stopWatchReducer}