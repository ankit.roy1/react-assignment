function start(){
    return{
        type:'START'
    }
}

function stop(){
    return{
        type:'STOP'
    }
}

function reset(){
    return {
        type:'RESET'
    }
}

function setMinutes(currTime){
    return {
        type:'SET_MINUTES'
    }
}
function setMinutesToZero(currTime){
    return {
        type:'SET_MINUTES_TO_ZERO'
    }
}
function setSeconds(){
    return{
        type:'SET_SECONDS'
    }
}
function setSecondsToZero(){
    return{
        type:'SET_SECONDS_TO_ZERO'
    }
}

function setMillisecond(){
    return{
        type:'SET_MILLISECOND'
    }
}
function setMillisecondToZero(){
    return{
        type:'SET_MILLISECOND_TO_ZERO'
    }
}

function lap(time){
    return{
        type:'LAP',
        payload:time
    }
}

function resetLaps(){
    return{
        type:'RESET_LAPS'
    }
}

export {
    start,
    stop,
    reset,
    lap,
    resetLaps,
    setMinutes,
    setSeconds,
    setMinutesToZero,
    setSecondsToZero,
    setMillisecond,
    setMillisecondToZero,
}