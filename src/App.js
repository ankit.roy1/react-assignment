import { useState } from "react";
import ButtonGroup from "./components/ButtonGroup";
import Timer from './components/Timer';
import Lap from './components/Lap';
function App() {
  return (
    <div className="app">
      <h2>STOP WATCH</h2>
      <Timer />
      <ButtonGroup />
      <Lap/>
    </div>
  );
}

export default App;
